'use strict';
/**
 *      PROGRAMACION ORIENTADA A OJBETOS
 */

/* ----------------------------- POO TRADICIONAL ---------------------------- */
let aBike = {
    /* ATRIBUTOS */
    color: "green",
    model: "BMX",
    maxSpeed: "50km",

    /* METODOS */
    changeColor: function (newColor) {
        this.color = newColor;
    }
};

console.log(aBike);