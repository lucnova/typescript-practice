/* Modulos básicos */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/* Importar Modulo de Forms */
import { FormsModule } from '@angular/forms'

/* Modulos propios */
import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';
import { BrandsComponent } from './brands/brands.component';
import { FoodsComponent } from './foods/foods.component';

@NgModule({
	declarations: [
		AppComponent,
		GameComponent,
		BrandsComponent,
		FoodsComponent
	],
	imports: [
        BrowserModule,
        FormsModule // Incluir Forms -> Esto trae el ngModel
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor() {
		console.log("[APP] Modulo Inicial Cargado");
	}
}