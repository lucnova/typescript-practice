import {
	Component
}
from '@angular/core';

@Component({
	selector: "brands",
	templateUrl: './brands.component.html'
})

export class BrandsComponent {
	public title: string;

	constructor() {
		this.title = "I'll list some brands...";

		console.log("[Brands] Brands Component Loaded");
	}
}