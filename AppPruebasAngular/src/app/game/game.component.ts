/* El primer paso es importar del objeto que se desea hacer; esto es un 
 *  Componente
 */
import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { Game } from '../models/game';

/**
 *  Hooks: Son eventos ejecutados en un momento dado del ciclo de vida del componente.
 *          Ejemplo, cambios, inicios del componente.
 *  ngOnInit -> Hook Inicial, se ejecuta solamente al cargar el componente.
 *  ngDoCheck -> Ejecutado cada vez que hay cambio en el componente o pagina
 */

/* Un decorador no usa ';' */
@Component({
	selector: "game", // Propiedad que define el nombre del componente (directiva)
    templateUrl: './game.component.html',    // Separar plantilla en un archivo html
	styleUrls: ['./game.component.css']
})

/* Implementa OnInit, DoCheck y OnDestroy */
export class GameComponent implements OnInit, DoCheck, OnDestroy {
    /* Propiedades de la clase GameComponent */
    public title: string;   // Título del Componente en HTML
    public gameList: Array<Game>;   // Lista de juegos a mostrar en HTML
    public gameCategories: Array<String>;
    public customText: string;

    private titleToggle: boolean = true;    // Para activar y desactivar el nombre del titulo

    /* Constructor por defecto */
	constructor() {
        this.customText = "Custom Text";
        /* Indicar el titulo */
        this.title = "Some Games I Find Interesting...";

        /* Llenar arreglo de juegos */
        this.gameList = [
            new Game('Left 4 Dead 2', 'Zombies!', 'FPS', '10', true, "M"),
            new Game("Demon's Crest", 'Demons!', 'Platform', '2', false, "T"),
            new Game("Zelda: Breath of The Wild", 'Zelda!', 'Adventure', '20', false, "T"),
            new Game("Mario RPG", 'Mario but RPG!', 'RPG', '10', true, "E"),
            new Game("Chrono Trigger", 'Japanese RPG!', 'RPG', '5', true, "T"),
            new Game("Tomato", 'Tomato?', 'RPG?', '500', true, "T")
        ];

        /* Llenar arreglo con categorias */
        this.gameCategories = [];
        for (const currGame of this.gameList) {
            /* Si ya existe entonces no insertarlo */
            if (this.gameCategories.indexOf(currGame.genre) == -1) {
                this.gameCategories.push(currGame.genre);
            }
        }
        
		console.info("[Game] Constructor Iniciado");
    }

    /* HOOKS */
    ngOnInit() {
        console.log("[Game] OnInit Iniciado");
    }

    ngDoCheck() {
        console.log("[Game] Cambio");
    }

    ngOnDestroy() {
        console.log("[Game] Destruido");
    }

    /* FUNCIONES */

    /* Alternar el título */
    changeTitle() {
        if (this.titleToggle == true) {
            this.title = "Some Games I Like...";
            this.titleToggle = false;
        }
        else {
            this.title = "Some Games I Find Interesting...";
            this.titleToggle = true;
        }
    }

    /* Agregar el género segun el valor del input bindeado */
    addGenre() {
        if (this.gameCategories.indexOf(this.customText) == -1) {
            this.gameCategories.push(this.customText);
        }
    }

    deleteGame(index) {
        this.gameList.splice(index, 1);
    }

    alertWrittenWord() {
        alert(this.customText);
    }
}