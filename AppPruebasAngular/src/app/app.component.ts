import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomConfig } from './models/customConfig';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
    public bgColor: string;
    public title: string;
    public description: string;
    public showVideoGames: boolean = true;

    constructor() {
        this.title = CustomConfig.title;
        this.description = CustomConfig.description;
        this.bgColor = CustomConfig.bgColor;
    }
    
    toggleVideoGames() {
        if(this.showVideoGames == true) {
            this.showVideoGames = false;
        }
        else {
            this.showVideoGames = true;
        }
    }
}