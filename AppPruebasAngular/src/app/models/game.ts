export class Game {
    /* Manera de ahorrar codigo: */

    /* Solo asigna los valores como:
        public name: string;

        this.name = name;
    */
   
    constructor(
        public name: string,
        public description: string,
        public genre:  string,
        public price: string,
        public stock: boolean,
        public rating: string
    ) {

    }
}