/**
 *  Interfaces: 
 *      Propiedades y métodos deben tener unas clases (obligatoriamente)
 * 
 * 
 * 
 */

interface ShirtBase {
    setColor(myColor : string);
    getColor();
}

class Shirt implements ShirtBase {
    /* ATRIBUTOS */
    private color: string;
    private brand: string;
    private size: string;

    constructor(aColor : string, aBrand : string, aSize : string) {
        this.color = aColor;
        this.brand = aBrand;
        this.size = aSize;
    }

    /* METODOS */
    /* SETTERS */
    public setColor(myColor: string) {
        this.color = myColor;
    }
    public setBrand(myBrand: string) {
        this.brand = myBrand;
    }
    public setSize(mySize: string) {
        this.size = mySize;
    }

    /* GETTERS */
    public getColor() : string {
        return this.color;
    }
    public getBrand() : string {
        return this.brand;
    }
    public getSize() : string {
        return this.size;
    }
}