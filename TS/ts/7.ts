/**
 *  Herencia: 
 *      Especie de clase padre, es una clase que puede definir
 *      otras clases heredando propiedades y métodos.
 * 
 * 
 */

/**
 *  Decorador:
 *      Añade una funcionalidad a una clase.
 *      Son experimentales.
 */

interface ShirtBase {
    setColor(myColor: string);
    setBrand(myBrand: string);
    setSize(mySize: string);

    getColor(): string;
    getBrand(): string;
    getSize(): string;
}

class Shirt implements ShirtBase {
    /* ATRIBUTOS */
    private color: string;
    private brand: string;
    private size: string;

    constructor() {
        this.color = "aColor";
        this.brand = "aBrand";
        this.size = "aSize";
    }

    /* METODOS */
    /* SETTERS */
    public setColor(myColor: string) {
        this.color = myColor;
    }
    public setBrand(myBrand: string) {
        this.brand = myBrand;
    }
    public setSize(mySize: string) {
        this.size = mySize;
    }

    /* GETTERS */
    public getColor(): string {
        return this.color;
    }
    public getBrand(): string {
        return this.brand;
    }
    public getSize(): string {
        return this.size;
    }
}

class Sweatshirt extends Shirt {
    private hoodie: boolean;

    constructor() {
        super(); // Llama al constructor padre
        this.hoodie = true;
    }

    public setHoodie(hasHoodie: boolean) {
        this.hoodie = hasHoodie;
    }

    public getHoodie(): boolean {
        return this.hoodie;
    }
}

let mySweatshirt = new Sweatshirt();
console.log(mySweatshirt);
