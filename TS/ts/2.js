/**
 *  TYPESCRIPT VARIABLES
 */
console.log("*** VARIABLES ***");
/* --------------------------------- STRING --------------------------------- */
var myString = "Hello";
//myString = 12;  // Esto da error
/* --------------------------------- NUMBER --------------------------------- */
var myNumber = 16;
/* -------------------------------- BOOLEANO -------------------------------- */
var myBool = true;
/* ----------------------------------- ANY ---------------------------------- */
var myAny = "As it is";
/* -------------------------------- ARREGLOS -------------------------------- */
var myArray = ["JS", "PHP", "C#"];
//let myArray: string[] = ["JS", "PHP", "C#"];   // Otra forma
//myAny = 12;   // No da error
console.log(myString, typeof myString);
console.log(myNumber, typeof myNumber);
console.log(myBool, typeof myBool);
console.log(myAny, typeof myAny);
console.log(myArray, typeof myArray);
console.log("*** MULTIPLES DATOS ***");
/* ----------------------------- MULTIPLES TIPOS ---------------------------- */
var myVar = "Hi";
console.log(myVar, typeof myVar);
myVar = 12;
console.log(myVar, typeof myVar);
console.log("*** VARS CUSTOM ***");
var myOtherVar = "2";
console.log(myOtherVar, typeof myOtherVar);
myOtherVar = 2;
console.log(myOtherVar, typeof myOtherVar);
