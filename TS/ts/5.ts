/**
 *      En JS no funciona correctamente, pero en Angular si.
 * 
 */

import {Shirt} from './shirt';

class Main {
    constructor () {
        console.log("Loaded");
    }

    getShirtInfo() {
        return new Shirt("red", "Nike", "XL");
    }

}

let main = new Main();