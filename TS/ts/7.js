/**
 *  Herencia:
 *      Especie de clase padre, es una clase que puede definir
 *      otras clases heredando propiedades y métodos.
 *
 *
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Shirt = /** @class */ (function () {
    function Shirt() {
        this.color = "aColor";
        this.brand = "aBrand";
        this.size = "aSize";
    }
    /* METODOS */
    /* SETTERS */
    Shirt.prototype.setColor = function (myColor) {
        this.color = myColor;
    };
    Shirt.prototype.setBrand = function (myBrand) {
        this.brand = myBrand;
    };
    Shirt.prototype.setSize = function (mySize) {
        this.size = mySize;
    };
    /* GETTERS */
    Shirt.prototype.getColor = function () {
        return this.color;
    };
    Shirt.prototype.getBrand = function () {
        return this.brand;
    };
    Shirt.prototype.getSize = function () {
        return this.size;
    };
    return Shirt;
}());
var Sweatshirt = /** @class */ (function (_super) {
    __extends(Sweatshirt, _super);
    function Sweatshirt() {
        var _this = _super.call(this) || this;
        _this.hoodie = true;
        return _this;
    }
    Sweatshirt.prototype.setHoodie = function (hasHoodie) {
        this.hoodie = hasHoodie;
    };
    Sweatshirt.prototype.getHoodie = function () {
        return this.hoodie;
    };
    return Sweatshirt;
}(Shirt));
var mySweatshirt = new Sweatshirt();
console.log(mySweatshirt);
