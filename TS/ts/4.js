/**
 *  Programacion Orientada a Objetos:
 *      Clases
 *          - Atributos
 *          - Metodos
 *
 */
var Shirt = /** @class */ (function () {
    function Shirt(aColor, aBrand, aSize) {
        this.color = aColor;
        this.brand = aBrand;
        this.size = aSize;
    }
    /* METODOS */
    /* SETTERS */
    Shirt.prototype.setColor = function (myColor) {
        this.color = myColor;
    };
    Shirt.prototype.setBrand = function (myBrand) {
        this.brand = myBrand;
    };
    Shirt.prototype.setSize = function (mySize) {
        this.size = mySize;
    };
    /* GETTERS */
    Shirt.prototype.getColor = function () {
        return this.color;
    };
    Shirt.prototype.getBrand = function () {
        return this.brand;
    };
    Shirt.prototype.getSize = function () {
        return this.size;
    };
    return Shirt;
}());
var myShirt = new Shirt("red", "Nike", "XL");
console.log(myShirt.getColor());
console.log(myShirt.getBrand());
console.log(myShirt.getSize());
