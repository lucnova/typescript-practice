"use strict";
/**
 *      En JS no funciona correctamente, pero en Angular si.
 *
 */
exports.__esModule = true;
var shirt_1 = require("./shirt");
var Main = /** @class */ (function () {
    function Main() {
        console.log("Loaded");
    }
    Main.prototype.getShirtInfo = function () {
        return new shirt_1.Shirt("red", "Nike", "XL");
    };
    return Main;
}());
var main = new Main();
