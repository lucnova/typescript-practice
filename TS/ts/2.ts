/** 
 *  TYPESCRIPT VARIABLES
 */

console.log("*** VARIABLES ***");
/* --------------------------------- STRING --------------------------------- */
let myString: string = "Hello";

//myString = 12;  // Esto da error

/* --------------------------------- NUMBER --------------------------------- */
let myNumber: number = 16;

/* -------------------------------- BOOLEANO -------------------------------- */
let myBool: boolean = true;

/* ----------------------------------- ANY ---------------------------------- */
let myAny: any = "As it is";

/* -------------------------------- ARREGLOS -------------------------------- */
let myArray: Array<string> = ["JS", "PHP", "C#"];

//let myArray: string[] = ["JS", "PHP", "C#"];   // Otra forma

//myAny = 12;   // No da error

console.log(myString, typeof myString);
console.log(myNumber, typeof myNumber);
console.log(myBool, typeof myBool);
console.log(myAny, typeof myAny);
console.log(myArray, typeof myArray)

console.log("*** MULTIPLES DATOS ***");
/* ----------------------------- MULTIPLES TIPOS ---------------------------- */
let myVar: string | number = "Hi";
console.log(myVar, typeof myVar)

myVar = 12;
console.log(myVar, typeof myVar)

console.log("*** VARS CUSTOM ***");
/* ------------------------ VARIABLES PERSONALIZADAS ------------------------ */
type alphaNumber = string | number;
let myOtherVar: alphaNumber = "2";
console.log(myOtherVar, typeof myOtherVar)

myOtherVar = 2;
console.log(myOtherVar, typeof myOtherVar)
